import 'package:flutter/material.dart';

class WrapDirectionHorizontal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("WrapDirectionHorizontal"),
      ),
      body: WrapBody(),
    );
  }
}


class WrapBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Wrap(
      alignment: WrapAlignment.start,
      direction: Axis.horizontal,
      spacing: 20.0, // gap between adjacent chips
      runSpacing: 10.0, // gap between lines
      children: <Widget>[
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('Cupcake')),
          label: Text('Cupcake'),
        ),
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('Donut')),
          label: Text('Donut'),
        ),
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('Eclair')),
          label: Text('Eclair'),
        ),
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('Froyo')),
          label: Text('Froyo'),
        ),
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('GingerBread')),
          label: Text('GingerBread'),
        ),
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('Honeycomb')),
          label: Text('Honeycomb'),
        ),
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('Ice Cream Sandwich')),
          label: Text('Ice Cream Sandwich'),
        ),
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('Jelly Bean')),
          label: Text('Jelly Bean'),
        ),
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('KitKat')),
          label: Text('KitKat'),
        ),
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('Lollipop')),
          label: Text('Lollipop'),
        ),
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('Marshmallow')),
          label: Text('Marshmallow'),
        ),
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('Nougat')),
          label: Text('Nougat'),
        ),
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('Oreo')),
          label: Text('Oreo'),
        ),
        Chip(
          avatar: CircleAvatar(backgroundColor: Colors.blue.shade900, child: Text('Pie')),
          label: Text('Pie'),
        ),
      ],
    );
  }
}