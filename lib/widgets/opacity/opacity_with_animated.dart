import 'package:flutter/material.dart';

class OpacityWithAnimated extends StatefulWidget {
  @override
  _OpacityWithAnimatedState createState() => _OpacityWithAnimatedState();
}

class _OpacityWithAnimatedState extends State<OpacityWithAnimated> {
  bool _visible = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("OpacityWithAnimated"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Container(
                width: 200.0,
                height: 200.0,
                color: Colors.red,
              ),
            ),
            AnimatedOpacity(
              opacity: _visible ? 1.0 : 0.0,
              duration: Duration(milliseconds: 500),
              child: Container(
                width: 200.0,
                height: 200.0,
                color: Colors.green,
              ),
            ),
            Container(
              child: Container(
                width: 200.0,
                height: 200.0,
                color: Colors.blue,
              ),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
          onPressed: (){
            setState(() {
              _visible = !_visible;
            });
          },
        icon: Icon(Icons.flip),
        label: Text("Toggle Opacity"),
      ),
    );
  }
}