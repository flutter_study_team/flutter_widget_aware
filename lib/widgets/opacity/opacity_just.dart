import 'package:flutter/material.dart';

class OpacityJust extends StatefulWidget {
  @override
  _OpacityJustState createState() {
    return _OpacityJustState();
  }
}

class _OpacityJustState extends State<OpacityJust> {
  var _visible = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("OpacityJust"),
      ),
      body: Container (
        alignment: FractionalOffset.center,
        child: Opacity(
          opacity: _visible ? 1.0 : 0.0,
          child: const Text('Now you see me, now you don\'t!',),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
          onPressed: (){
            setState(() {
              _visible = false;
            });
          },
          label: Text("Hide Button")
      ),
    );
  }
}