import 'package:flutter/material.dart';

class DraggableWidget extends StatefulWidget {
  @override
  _DraggableWidgetState createState() => _DraggableWidgetState();
}

class _DraggableWidgetState extends State<DraggableWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("DraggableWidget"),
      ),
      body: Builder(
        builder: (context) => Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  createDraggable(Colors.blue, "blue"),
                  createDraggable(Colors.amber, "amber"),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  createDragtarget(context, Colors.blue, "blue"),
                  createDragtarget(context, Colors.amber, "amber"),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}


Widget createDraggable(MaterialColor color, String data) => Draggable(
      child: Container(
        height: 100,
        width: 100,
        color: color,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Text(
              "Drag me to my color name",
              style: TextStyle(color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
      feedback: Container(
        height: 100,
        width: 100,
        color: color,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Material(
              type: MaterialType.transparency,
              child: Text(
                "I am being dragged",
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ),
      childWhenDragging: Container(
        height: 100,
        width: 100,
        color: Colors.grey,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: Text(
              "Original place",
              style: TextStyle(color: Colors.black),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
      data: data,
      onDragStarted: () => print("Drag Started"),
      onDragCompleted: () => print("Drag Completed"),
    );


Widget createDragtarget(BuildContext context, MaterialColor color, String dataOfDragged) =>
    DragTarget(
      builder: (context, List<String> candidateData, rejectedData) {
        return Container(
          height: 100,
          width: 100,
          color: color,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Text(
                "Drag block here",
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        );
      },
      onWillAccept: (data) {
        if (data == dataOfDragged) {
          return true;
        } else {
          return false;
        }
      },

      onAccept: (data) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Accepted!!'),
          backgroundColor: Colors.green,
          duration: Duration(seconds: 1),
        ));
      },

      onLeave: (data) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Rejected!!'),
          backgroundColor: Colors.red,
          duration: Duration(seconds: 1),
        ));
      },
    );
