import 'package:flutter/material.dart';

class ValueListenableBuilderWidget extends StatefulWidget {
  @override
  _ValueListenableBuilderWidgetState createState() =>
      _ValueListenableBuilderWidgetState();
}

class _ValueListenableBuilderWidgetState extends State<ValueListenableBuilderWidget> {
  final ValueNotifier<int> _counter = ValueNotifier<int>(2);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ValueListenableBuilderWidget"),
      ),
      body: Center(
        child: ValueListenableBuilder(
          builder: (BuildContext context, int value, Widget child) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Text(
                    '$value',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 34.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'The number is now ',
                    style: TextStyle(
                        fontWeight: FontWeight.normal, fontSize: 18.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: value.remainder(2) == 0
                        ? Text(
                            "Even",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 24.0),
                          )
                        : Text(
                            "Odd",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 24.0),
                          ),
                  ),
                ),
                child,
              ],
            );
          },
          valueListenable: _counter,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("I dont care about the value"),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.plus_one),
        backgroundColor: Colors.blue,
        foregroundColor: Colors.white,
        onPressed: () => _counter.value += 1,
      ),
    );
  }
}
