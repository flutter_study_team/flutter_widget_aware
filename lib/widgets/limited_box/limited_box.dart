import 'package:flutter/material.dart';

class LimitedBoxWidget extends StatefulWidget {
  @override
  _LimitedBoxWidgetState createState() => _LimitedBoxWidgetState();
}

class _LimitedBoxWidgetState extends State<LimitedBoxWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(
            'LimitedBox Widget',
            style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
            ),
          ),
        ),
        body: MediaQuery.of(context).orientation == Orientation.portrait
            ? portraitLayout(context)
            : landscapeLayout(context));
  }
}

Widget portraitLayout(context) => ListView(
      children: <Widget>[
        LimitedBox(
          maxHeight: 100,
          child: Container(
            color: Colors.amber,
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "I am wrapped inside a Limited Box with maxHeight set to 200\n"
                      "This is necessary as my parent (ListView) is unconstrained\n"
                      "Flip screen to see the other case",
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.center,
                ),
              ),
          ),
        ),
        ),
      ],
    );

Widget landscapeLayout(context) => Container(
      color: Colors.indigo,
      child: LimitedBox(
        maxWidth: 20,
        child: Container(
          color: Colors.amber,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "I am wrapped inside a Limited Box with maxWidth set to 20\n"
                "But as my parent (Container) is already constrained, LimitedBox is respecting\n"
                "those constrains.",
                style: TextStyle(color: Colors.black),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ),
    );
