import 'dart:ui';

import 'package:flutter/material.dart';

class BackDropFilterWidget extends StatefulWidget {
  @override
  _BackDropFilterWidgetState createState() => _BackDropFilterWidgetState();
}

class _BackDropFilterWidgetState extends State<BackDropFilterWidget> {
  var _blurValue = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("BackdropFilterWidget"),
      ),
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(24),
                height: 200,
                width: 200,
                child: FlutterLogo(
                  colors: Colors.lightBlue,
                  textColor: Colors.white,
                ),
              ),

              Positioned.fill(
                child: BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: _blurValue,
                    sigmaY: _blurValue,
                  ),
                  child: Container(
                    color: Colors.black.withOpacity(0.0),
                  ),
                ),
              ),
            ],
          ),
          Slider(
            value: _blurValue,
            activeColor: Colors.lightBlue,
            inactiveColor: Colors.lightBlue[50],
            min: 0.0,
            max: 10.0,
            divisions: 10,
            label: '${_blurValue.truncate()}',
            onChanged: (double value) {
              setState(() {
                _blurValue = value;
              });
            },
          ),
          Container(
            margin: EdgeInsets.all(12),
            child: Text(
              'Drag the slider above to change the blur effect on the FLutter Widget',
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.grey[400],
                  fontSize: 14.0,
                  fontStyle: FontStyle.italic,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
