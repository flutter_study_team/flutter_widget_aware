import 'package:flutter/material.dart';

class TooltipWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("TooltipWidget"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Wrap(
              spacing: 20,
              runAlignment: WrapAlignment.center,
              alignment: WrapAlignment.center,
              runSpacing: 48,
              children: <Widget>[
                Tooltip(
                  message: "Default Tooltip",
                  child: Container(
                    height: 100,
                    width: 100,
                    color: Colors.green,
                    child: Center(
                      child: Text(
                        'Long Press',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14.0,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
                Tooltip(
                  message: "Tooltip with an offset",
                  verticalOffset: 54,
                  child: Container(
                    height: 100,
                    width: 100,
                    color: Colors.orange,
                    child: Center(
                      child: Text(
                        'Long Press',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14.0,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
                Tooltip(
                  message: "Tooltip with custom height",
                  height: 50,
                  verticalOffset: 54,
                  child: Container(
                    height: 100,
                    width: 100,
                    color: Colors.red,
                    child: Center(
                      child: Text(
                        'Long Press',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 14.0,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
                IconButton(
                  iconSize: 100,
                  onPressed: () {},
                  icon: Icon(Icons.insert_emoticon),
                  tooltip: "Default Icon Tooltip",
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
