import 'package:flutter/material.dart';

class CustomPaintWidget extends StatefulWidget {
  @override
  _CustomPaintWidgetState createState() => _CustomPaintWidgetState();
}

class _CustomPaintWidgetState extends State<CustomPaintWidget> {
  var _bgColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("CustomPaintWidget"),
      ),
      body: CustomPaint(
        size: Size(MediaQuery.of(context).size.width, MediaQuery.of(context).size.height),
        painter: MyPainter(_bgColor),
      ),
    );
  }
}


class MyPainter extends CustomPainter {

  MyPainter(this.color);

  final Color color;

  @override
  void paint(Canvas canvas, Size size) {
    var rect = Offset.zero & size;
    Paint rectPaint = Paint()..color = color;
    canvas.drawRect(
      rect,
      rectPaint,
    );

    var width = size.width;
    var height = size.height;

    canvas.drawLine(Offset(width / 2.5, height / 6), Offset(width / 20, height / 2), _paint(Colors.amber));
    canvas.drawLine(Offset(width / 1.7, height / 6), Offset(width / 2.5, height / 6), _paint(Colors.red));
    canvas.drawLine(Offset(width / 6.5, height / 1.7), Offset(width / 1.7, height / 6), _paint(Colors.green));
    canvas.drawLine(Offset(width / 6.5, height / 1.7), Offset(width / 20, height / 2), _paint(Colors.blue));
    canvas.drawLine(Offset(width / 2.5, height / 2), Offset(width / 1.7, height / 2), _paint(Colors.blue));
    canvas.drawLine(Offset(width / 2.5, height / 2), Offset(width / 4.4, height / 1.5), _paint(Colors.amber));
    canvas.drawLine(Offset(width / 1.7, height / 2), Offset(width / 3.1, height / 1.33), _paint(Colors.green));
    canvas.drawLine(Offset(width / 4.4, height / 1.5), Offset(width / 2.5, height / 1.2), _paint(Colors.red));
    canvas.drawLine(Offset(width / 1.7, height / 1.2), Offset(width / 2.5, height / 1.2), _paint(Colors.amber));
    canvas.drawLine(Offset(width / 1.7, height / 1.2), Offset(width / 3.17, height / 1.72), _paint(Colors.blue));
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }

  _paint(Color color) {
    return Paint()
      ..color = color
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 4.0;
  }
}
