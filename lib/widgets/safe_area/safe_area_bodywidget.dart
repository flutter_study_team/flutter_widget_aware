//import 'package:flutter/material.dart';
//
//class SafeAreaBodyWidget extends StatefulWidget {
//  @override
//  _SafeAreaBodyWidgetState createState() => _SafeAreaBodyWidgetState();
//}
//
//class _SafeAreaBodyWidgetState extends State<SafeAreaBodyWidget> {
//  bool _isEnabled = true;
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      body: Container(
//        child: Align(
//          alignment: Alignment.bottomLeft,
//          child: SafeArea(
//            top: _isEnabled,
//            bottom: _isEnabled,
//            left: _isEnabled,
//            right: _isEnabled,
//            child: Column(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              mainAxisSize: MainAxisSize.max,
//              children: <Widget>[
//                Container(
//                  width: MediaQuery.of(context).size.width,
//                  color: Colors.blue,
//                  child: Text(
//                    "A widget that insets its child by sufficient padding to avoid intrusions by the operating system." +
//                        "For example, this will indent the child by enough to avoid the status bar at the top of the screen." +
//                        "It will also indent the child by the amount necessary to avoid The Notch on the iPhone X, or other similar creative physical features of the display.",
//                    textAlign: TextAlign.center,
//                    style: TextStyle(color: Colors.white),
//                  ),
//                ),
//
//                ///Press this button to toggle the value of _isEnabled variable
//                RaisedButton(
//                  textColor: Colors.white,
//                  color: Colors.indigo,
//                  onPressed: () => setState(() {
//                    _isEnabled == true ? _isEnabled = false : _isEnabled = true;
//                  }),
//                  child: Text(_isEnabled ? "Disable SafeArea" : "Enable SafeArea"),
//                ),
//
//                Container(
//                  width: MediaQuery.of(context).size.width,
//                  color: Colors.blue,
//                  child: Text(
//                    "This widget is above safe area",
//                    textAlign: TextAlign.center,
//                    style: TextStyle(color: Colors.white),
//                  ),
//                ),
//              ],
//            ),
//          ),
//        ),
//      ),
//    );
//  }
//}
