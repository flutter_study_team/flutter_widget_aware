import 'package:flutter/material.dart';

class AnimatedGradientTransform extends StatefulWidget {
  @override
  _AnimatedGradientTransformState createState() {
    return _AnimatedGradientTransformState();
  }
}

class _AnimatedGradientTransformState extends State<AnimatedGradientTransform> {

  var top = FractionalOffset.topCenter;
  var bottom = FractionalOffset.bottomCenter;
  var list = [
    Colors.lightGreen,
    Colors.redAccent,
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("AnimatedGradientTransform"),
      ),
      body: Center(
        child: AnimatedContainer(
          height: 300.0,
          width: 300.0,
          duration: Duration(seconds: 1),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              gradient: LinearGradient(
                begin: top,
                end: bottom,
                colors: list,
                stops: [0.0, 1.0],
              ),
              color: Colors.lightGreen
          ),
        ),
      ),

      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
          onPressed: (){
            setState(() {
              top = FractionalOffset.bottomLeft;
              bottom = FractionalOffset.topRight;
              list = [
                Colors.blueAccent, Colors.yellowAccent
              ];
            });
          },
          icon: Icon(Icons.update),
          label: Text("Transform")),
    );
  }
}