import 'package:flutter/material.dart';

class AnimatedAlignment extends StatefulWidget {
  @override
  _AnimatedAlignmentState createState() {
    return _AnimatedAlignmentState();
  }
}

class _AnimatedAlignmentState extends State<AnimatedAlignment> {

  var _alignment = Alignment.bottomCenter;
  var color = Colors.yellow;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("AnimatedAlignment"),
      ),
      body: AnimatedContainer(
        padding: EdgeInsets.all(10.0),
        duration: Duration(seconds: 1),
        alignment: _alignment,
        color: color,
        child: Container(
          child: Icon(Icons.airplanemode_active, size: 50.0, color: Colors.yellow,),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
          backgroundColor: Colors.blueAccent,
          onPressed: (){
            setState(() {
              _alignment = Alignment.center;
              color = Colors.blue;
            });
          },
          icon: Icon(Icons.airplanemode_active),
          label: Text("Take Flight")
      ),
    );
  }
}