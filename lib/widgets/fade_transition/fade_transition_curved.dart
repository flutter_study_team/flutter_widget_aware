import 'dart:math';

import 'package:flutter/material.dart';

class FadeTransitionCurved extends StatefulWidget {
  @override
  _FadeTransitionCurvedState createState() => _FadeTransitionCurvedState();
}

class _FadeTransitionCurvedState extends State<FadeTransitionCurved> with TickerProviderStateMixin {

  AnimationController controller;
  Animation<double> animation;

  initState() {
    super.initState();
    controller = AnimationController(duration: const Duration(milliseconds: 1000), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.easeIn);
    controller.forward();
  }


  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FadeTransitionCurved'),
      ),
      body: Container(
        alignment: FractionalOffset.center,
        child: FadeTransition(
          opacity: animation,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children:[
              Icon(Icons.check_circle, size: 100.0,color: Colors.blue,),
            ]
          ),
        ),
      ),
    );
  }


}

