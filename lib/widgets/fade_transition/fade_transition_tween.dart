import 'package:flutter/material.dart';

class FadeTransitionTween extends StatefulWidget {
  @override
  _FadeTransitionTweenState createState() => _FadeTransitionTweenState();
}

class _FadeTransitionTweenState extends State<FadeTransitionTween> with SingleTickerProviderStateMixin {

  AnimationController _controller;
  Animation _animation;
  CurvedAnimation _curve;


  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );

    _curve = CurvedAnimation(parent: _controller, curve: Curves.easeIn);

    _animation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(_curve);

    _animation.addStatusListener((status) {
      if (status == AnimationStatus.completed)
        _controller.reverse();
      else if (status == AnimationStatus.dismissed) _controller.forward();
    });

    super.initState();
  }


  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("FadeTransitionTween"),
      ),
      body: Center(
        child: FadeTransition(
          opacity: _animation,
          child: Icon(
            Icons.favorite,
            color: Colors.red,
            size: 300,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Colors.lightBlue,
        onPressed: () => setState(() {
          _controller.forward();
        }),
        icon: Icon(
          Icons.movie_filter,
          color: Colors.white,
        ),
        label: Text(
          "Animate",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

}

