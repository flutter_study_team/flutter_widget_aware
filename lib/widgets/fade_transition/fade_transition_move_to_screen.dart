import 'package:flutter/material.dart';

class FadeTransitionMoveToScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("FadeTransitionMoveToScreenPage"),
      ),
      body: FadeTransitionMoveToScreen(),
    );
  }
}




class FadeTransitionMoveToScreen extends StatelessWidget {

  static const List<String> _products = const [
    '/WithAnimation',
    '/WithOutAnimation',
  ];


  /*
   */
  ScaleTransition getScaleTransition(Animation<double> animation, Widget child) {
    return ScaleTransition(
      scale: Tween<double>(
        begin: 0.0,
        end: 1.0,
      ).animate(
        CurvedAnimation(
          parent: animation,
          curve: Interval(
            0.00,
            0.50,
            curve: Curves.linear,
          ),
        ),
      ),
      child: ScaleTransition(
        scale: Tween<double>(
          begin: 1.5,
          end: 1.0,
        ).animate(
          CurvedAnimation(
            parent: animation,
            curve: Interval(
              0.50,
              1.00,
              curve: Curves.linear,
            ),
          ),
        ),
        child: child,
      ),
    );
  }


  /*
   */
  SlideTransition getSlideTransition(Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
    return SlideTransition(
      position: Tween<Offset>(
        begin: const Offset(-1.0, 0.0),
        end: Offset.zero,
      ).animate(animation),
      child: SlideTransition(
        position: Tween<Offset>(
          begin: Offset.zero,
          end: const Offset(-1.0, 0.0),
        ).animate(secondaryAnimation),
        child: child,
      ),
    );
  }


  /*
   */
  FadeTransition getFadeTransition(Animation<double> animation, Widget child) {
    return FadeTransition(
      opacity: animation,
      child: child,
    );
  }

  /*
   */
  _pushWidgetWithFade(BuildContext context, Widget widget) {
    Navigator.of(context).push(PageRouteBuilder(transitionsBuilder: (context, animation, secondaryAnimation, child) =>
//        getFadeTransition(animation, child,),
      getScaleTransition(animation, child,),
//        getSlideTransition(animation, secondaryAnimation, child,),
        pageBuilder: (BuildContext context, Animation animation, Animation secondaryAnimation) {
          return widget;
        }),
    ).then((value) {
      if (null != value) {
        print("_pushWidgetWithFade >> value : " + value.toString());
      }
    });
  }


  /*
   */
  _pushWidgetWithOutFade(BuildContext context, Widget widget) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => widget),);
  }


  Widget _buildProductItem(BuildContext context, int index) {
    return Container(
      child: FlatButton(
        onPressed: () {
          print("_buildProductItem >> _products > index.toString() :: " + index.toString() + " , _products[index] : " + _products[index]);
          if (index == 0) {
            _pushWidgetWithFade(context, FadeTransitionWithFadeToScreenA());
          } else if (index == 1) {
            _pushWidgetWithOutFade(context, FadeTransitionWithOutFadeToScreenA());
          }
        },
        child: Align(
          alignment: Alignment.centerLeft,
          child: Container(
            padding: EdgeInsets.only(top: 22, bottom: 22, right: 22),
            child: Row(children: [
              RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: _products[index],
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Colors.grey,
                        fontStyle: FontStyle.normal,
                        fontSize: 18.0,
                      ),
                    ),
                  ],
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: _buildProductItem,
      itemCount: _products.length,
    );
  }
}


/*
 *
 */
class FadeTransitionWithFadeToScreenA extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FadeTransitionWithFadeToScreenA'),
      ),
      body: Container(
        child: Center(
          child: RichText(
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                  text: "FadeTransitionWithFadeToScreenA",
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.grey,
                    fontStyle: FontStyle.normal,
                    fontSize: 18.0,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}


/*
 *
 */
class FadeTransitionWithOutFadeToScreenA extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FadeTransitionWithOutFadeToScreenA'),
      ),
      body: Container(
        child: Center(
          child: RichText(
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                  text: "FadeTransitionWithOutFadeToScreenA",
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.grey,
                    fontStyle: FontStyle.normal,
                    fontSize: 18.0,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}