import 'dart:async';

import 'package:flutter/material.dart';


class InheritedWidgett extends StatefulWidget {
  @override
  _NamePageInheritedWidgetState createState() => new _NamePageInheritedWidgetState();
}

class NameInheritedWidget extends InheritedWidget {
  const NameInheritedWidget({
    Key key,
    this.name,
    Widget child,
  }) : super(
    key: key,
    child: child,
  );

  final String name;

  @override
  bool updateShouldNotify(NameInheritedWidget old) {
    print('In updateShouldNotify');
    return name != old.name;
  }

  static NameInheritedWidget of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(NameInheritedWidget);
  }
}


class _NamePageInheritedWidgetState extends State<InheritedWidgett> {
  String name = 'Placeholder';
  Timer _timerLink;

  _get() async {
    _timerLink = new Timer(const Duration(milliseconds: 850), () {
      var name = "received image!!";
//      var name = "Placeholder";
      this.name = name;
      setState(() => this.name = name);
    });
  }

  @override
  void initState() {
    super.initState();
    _get();
  }

  @override
  void dispose() {
    if (_timerLink != null) {
      _timerLink.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return NameInheritedWidget(
      name: name,
      child: const IntermediateWidget(),
    );
  }
}


class IntermediateWidget extends StatelessWidget {
  const IntermediateWidget();
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: const NameWidget(),
      ),
    );
  }
}


class NameWidget extends StatelessWidget {
  const NameWidget();
  @override
  Widget build(BuildContext context) {
    final inheritedWidget = NameInheritedWidget.of(context);
    print('NameWidget >> inheritedWidget : ' + inheritedWidget.name);

    return Text(
      inheritedWidget.name,
      style: Theme.of(context).textTheme.display1,
    );
  }
}

