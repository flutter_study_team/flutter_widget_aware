import 'package:flutter/material.dart';

class LayoutBuilderWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("LayoutBuilderWidget"),
      ),
      body: Center(
        child: LayoutBuilder(
          builder: (context, constraints) {
            if (constraints.maxWidth < 400 ) {
              return Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                    Expanded(
                      child: Container(color: Colors.lightBlue),
                      flex: 1,
                    ),
                    Expanded(
                      child: Container(color: Colors.indigo),
                      flex: 1,
                    )
                ],
              );
            }else{
              return Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Container(color: Colors.lightBlue),
                    flex: 1,
                  ),
                  Expanded(
                    child: Container(color: Colors.indigo),
                    flex: 1,
                  ),
                  Expanded(
                    child: Container(color: Colors.pink),
                    flex: 1,
                  ),
                  Expanded(
                    child: Container(color: Colors.blue),
                    flex: 1,
                  )
                ],
              );
            }
          },
        ),
      ),
    );
  }
}
