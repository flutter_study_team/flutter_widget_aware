import 'package:flutter/material.dart';

class DismissibleWidget extends StatefulWidget {
  @override
  _DismissibleWidgetState createState() => _DismissibleWidgetState();
}

class _DismissibleWidgetState extends State<DismissibleWidget> {
  final items = List<String>.generate(30, (i) => "Item ${i + 1}");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("DismissibleWidget"),
      ),
      body: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          final item = items[index];

          return Dismissible(
            key: Key(item),
            onDismissed: (direction) {
              setState(() {
                items.removeAt(index);
              });

              Scaffold.of(context).showSnackBar(SnackBar(content: Text("$item dismissed")));
            },
            background: Container(
              padding: EdgeInsets.only(left: 12),
              alignment: Alignment.centerLeft,
              color: Colors.green,
              child: Row(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text(
                      "Positive Action",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  Icon(
                    Icons.check,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
            secondaryBackground: Container(
              padding: EdgeInsets.only(right: 12),
              alignment: Alignment.centerRight,
              color: Colors.red,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text(
                      "Negative Action",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  Icon(
                    Icons.cancel,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
            child: ListTile(
              leading: Icon(Icons.swap_horiz),
              title: Text('$item'),
            ),
          );
        },
      ),
    );
  }
}
