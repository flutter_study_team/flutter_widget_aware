import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_widget_aware/common/constants_color.dart';

class PageViewOnListItem extends StatefulWidget {
  @override
  _PageViewOnListItemState createState() => _PageViewOnListItemState();
}

class _PageViewOnListItemState extends State<PageViewOnListItem> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PageViewOnListItem',),
      ),
      body: ListView.builder(
        itemCount: 20,
        itemBuilder: (BuildContext context, int index) {
          return PageViewOnList(index: index,);
        },
      ),
    );
  }
}


// ignore: must_be_immutable
class PageViewOnList extends StatefulWidget {
  int index = 0;
  PageViewOnList({Key key, @required this.index}) : super(key: key);
  @override
  State createState() => _PageViewOnListState();
}


class _PageViewOnListState extends State<PageViewOnList> {

  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;
  final _controller = PageController();
  int _pageCount = 5;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("widget.index :: " + widget.index.toString());
    return Container(
      height: 150.0,
      child: (widget.index % 2 == 1) ? Center(
        child: Text(widget.index.toString()),
      ) : Stack(
        children: <Widget>[
          PageView.builder(
            physics: BouncingScrollPhysics(),
            controller: _controller,
            itemCount: _pageCount, // Can be null
            itemBuilder: (BuildContext context, int index) {
              int position = (index <= _pageCount) ? index % _pageCount : -1;
              if (position < 0) {
                return null;
              }
              return ViewItem(index: index);
            },
          ),

          Positioned(
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: Container(
              padding: const EdgeInsets.all(2.0),
              child: Center(
                child: DotsIndicator(
                  controller: _controller,
                  itemCount: _pageCount,
                  onPageSelected: (int page) {
                    _controller.animateToPage(
                      page,
                      duration: _kDuration,
                      curve: _kCurve,
                    );
                  },
                ),
              ),
            ),
          ),

        ],
      ),
    );
  }
}


// ignore: must_be_immutable
class ViewItem extends StatelessWidget {
  int index;
  ViewItem({Key key, @required this.index}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      color: baseColor,
      child: Center(
        child: Text("view item : " + index.toString()),
      ),
    );
  }
}

class DotsIndicator extends AnimatedWidget {
  DotsIndicator({
    this.controller,
    this.itemCount,
    this.onPageSelected,
    this.color: Colors.white,
  }) : super(listenable: controller);

  final PageController controller;

  final int itemCount;

  final ValueChanged<int> onPageSelected;

  final Color color;

  static const double _kDotSize = 5.0;
  static const double _kMaxZoom = 2.0;
  static const double _kDotSpacing = 25.0;

  Widget _buildDot(int index) {
    double selectedness = Curves.easeOut.transform(
      max(
        0.0,
        1.0 - ((controller.page ?? controller.initialPage) - index).abs(),
      ),
    );
    double zoom = 1.0 + (_kMaxZoom - 1.0) * selectedness;
    return Container(
      width: _kDotSpacing,
      child: Center(
        child: Material(
          color: color,
          type: MaterialType.circle,
          child: Container(
            width: _kDotSize * zoom,
            height: _kDotSize * zoom,
            child: InkWell(
              onTap: () => onPageSelected(index),
            ),
          ),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List<Widget>.generate(itemCount, _buildDot),
    );
  }
}