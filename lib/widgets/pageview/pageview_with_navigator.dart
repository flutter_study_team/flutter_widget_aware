
import 'package:flutter/material.dart';
import 'package:flutter_widget_aware/common/constants_color.dart';

class PageViewWithNavigor extends StatefulWidget {
  @override
  _PageViewWithNavigorState createState() => _PageViewWithNavigorState();
}

class _PageViewWithNavigorState extends State<PageViewWithNavigor> {

  int _page = 0;
  PageController _pageController;

  List<Widget> _getList() {
    return <Widget>[
      PageViewItemAWidget(),
      PageViewItemBWidget(),
      PageViewItemCWidget(),
    ];
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void _navigationTapped(int page) {
    _pageController.animateToPage(page, duration: const Duration(milliseconds: 300), curve: Curves.ease);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PageViewWithNavigor'),
      ),
      body: Container(
        child: PageView(
          children: _getList(),
          scrollDirection: Axis.horizontal,
          controller: _pageController,
          onPageChanged: (int index) {
            print("onPageChanged > index : " + index.toString());
            setState(() {
              _page = index;
            });
          },
        ),
      ),
      bottomNavigationBar : BottomNavigationBar(
        type: BottomNavigationBarType.shifting,
        items: _getNavBarItems(),
        onTap: _navigationTapped,
        currentIndex: _page,
      ),
    );
  }


  List<BottomNavigationBarItem> _getNavBarItems() {
    return [
      BottomNavigationBarItem(
          icon: Icon(Icons.thumb_up, size: 25, color: Colors.white,),
          title: RichText(
            overflow: TextOverflow.ellipsis,
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                  text: "A",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ],
            ),
          ),
          backgroundColor : baseColor),
      BottomNavigationBarItem(
          icon: Icon(Icons.add_to_queue, size: 25, color: Colors.white,),
          title: RichText(
            overflow: TextOverflow.ellipsis,
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                  text: "B",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ],
            ),
          ),
          backgroundColor : baseColor),
      BottomNavigationBarItem(
          icon: Icon(Icons.show_chart, size: 25, color: Colors.white,),
          title: RichText(
            overflow: TextOverflow.ellipsis,
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                  text: "C",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 12.0,
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ],
            ),
          ),
          backgroundColor : baseColor),
    ];
  }
}


class PageViewItemAWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Icon(Icons.thumb_up, size: 100, color: Colors.black,),
    );
  }
}

class PageViewItemBWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Icon(Icons.add_to_queue, size: 100, color: Colors.black,),
    );
  }
}

class PageViewItemCWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Icon(Icons.show_chart, size: 100, color: Colors.black,),
    );
  }
}