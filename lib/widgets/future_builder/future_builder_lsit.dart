import 'package:flutter/material.dart';

class FutureBuilderList extends StatefulWidget {
  @override
  _FutureBuilderListState createState() => _FutureBuilderListState();
}

class _FutureBuilderListState extends State<FutureBuilderList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FutureBuilderList',),
      ),
      body: ListView.builder(
        itemCount: 1000,
        itemBuilder: (BuildContext context, int index) {
          return FutureBuilder(
            future: getFuture(), // <--- get a future
            builder: _resultBuild,
          );
        },
      ),
    );
  }

  Widget _resultBuild(BuildContext context, AsyncSnapshot snapshot) {
    return Container(
      height: 80.0,
      child: snapshot.hasData ? snapshot.data : Text('loading..'),
    );
  }

  Future<Widget> getFuture() {
    return Future.delayed(Duration(seconds: 2), () => Text('Hello World!'));
  }

}