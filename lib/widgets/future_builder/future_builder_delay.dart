import 'dart:math';

import 'package:flutter/material.dart';

class FutureBuilderDelay extends StatefulWidget {
  @override
  _FutureBuilderDelayState createState() => _FutureBuilderDelayState();
}

class _FutureBuilderDelayState extends State<FutureBuilderDelay> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FutureBuilderDelay',),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: FutureBuilder(
          future: getFutureData(),
          builder: _resultBuild,
        ),
      ),
    );
  }

  Widget _resultBuild(BuildContext context, AsyncSnapshot snapshot) {
    Text text = const Text('');
    if (snapshot.connectionState == ConnectionState.waiting) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    else if (snapshot.connectionState == ConnectionState.done) {
      if (snapshot.hasError) {
        text = Text(snapshot.error);
      } else if (snapshot.hasData) {
        text = Text(snapshot.data);
      } else {
        text = const Text('empty');
      }
    }
    return Center(
      child: Padding(padding: const EdgeInsets.all(16.0), child: text),
    );
  }

  Future<String> getFutureData() async =>
    await Future.delayed(Duration(seconds: 1), () {
      return 'Data Received';
    }
  );

}