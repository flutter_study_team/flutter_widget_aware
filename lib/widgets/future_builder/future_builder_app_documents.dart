import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';


class FutureBuilderAppDocuments extends StatefulWidget {
  @override
  _FutureBuilderAppDocumentsState createState() => _FutureBuilderAppDocumentsState();
}

class _FutureBuilderAppDocumentsState extends State<FutureBuilderAppDocuments> {
  Future<Directory> _appDocumentsDirectory;

  void _requestAppDocumentsDirectory() {
    setState(() {
      _appDocumentsDirectory = getApplicationDocumentsDirectory();
    });
  }

  @override
  void initState() {
    super.initState();
    _requestAppDocumentsDirectory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FutureBuilderAppDocuments',),
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        alignment: FractionalOffset.center,
        child: FutureBuilder<Directory>(
          future: _appDocumentsDirectory,
          builder: _buildDirectory,
        ),
      ),
    );
  }

  Widget _buildDirectory(BuildContext context, AsyncSnapshot<Directory> snapshot) {
    Text text = const Text('');
    if (snapshot.connectionState == ConnectionState.done) {
      if (snapshot.hasError) {
        text = Text('Error: ${snapshot.error}');
      } else if (snapshot.hasData) {
        text = Text('path: ${snapshot.data.path}');
      } else {
        text = const Text('path unavailable');
      }
    }
    return Padding(padding: const EdgeInsets.all(16.0), child: text);
  }

}