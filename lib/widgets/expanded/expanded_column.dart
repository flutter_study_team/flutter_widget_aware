import 'package:flutter/material.dart';


/*
 * https://api.flutter.dev/flutter/widgets/Expanded-class.html
 */
class ExpandedColumn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("ExpandedColumn"),
      ),
      body: ExpandedColumnBody(),
    );
  }
}


class ExpandedColumnBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  color: Colors.red,
                  height: 100,
                  child: Center(child: Text("flex: 2"),),
                ),
              ),
              Container(
                color: Colors.blue,
                height: 100,
                width: 50,
              ),
              Expanded(
                flex: 1,
                child: Container(
                  color: Colors.red,
                  height: 100,
                  child: Center(child: Text("flex: 1"),),
                ),
              ),
              Container(
                color: Colors.blue,
                height: 100,
                width: 50,
              ),
              Expanded(
                flex: 3,
                child: Container(
                  color: Colors.red,
                  height: 100,
                  child: Center(child: Text("flex: 3"),),
                ),
              ),
            ],
          ),

          Row(
            children: <Widget>[
              Expanded(
                flex: 8,
                child: Container(
                  color: Colors.yellow,
                  height: 100,
                  child: Center(child: Text("flex: 8"),),
                ),
              ),
              Container(
                color: Colors.blue,
                height: 100,
                width: 50,
              ),
              Expanded(
                flex: 2,
                child: Container(
                  color: Colors.yellow,
                  height: 100,
                  child: Center(child: Text("flex: 2"),),
                ),
              ),
            ],
          ),


          Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  color: Colors.red,
                  height: 100,
                  child: Center(child: Text("flex: 1"),),
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  color: Colors.grey,
                  height: 100,
                  child: Center(child: Text("flex: 2"),),
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  color: Colors.blue,
                  height: 100,
                  child: Center(child: Text("flex: 3"),),
                ),
              ),
              Expanded(
                flex: 4,
                child: Container(
                  color: Colors.green,
                  height: 100,
                  child: Center(child: Text("flex: 4"),),
                ),
              ),
            ],
          ),

        ],
      ),
    );
  }
}