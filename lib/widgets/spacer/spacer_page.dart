import 'package:flutter/material.dart';

/*
 * https://www.youtube.com/watch?v=7FJgd7QN1zI
 * https://api.flutter.dev/flutter/widgets/Spacer-class.html
 */
class SpacerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SpacerPage"),
      ),
      body: SpacerPageBody(),
    );
  }
}


class SpacerPageBody extends StatelessWidget {
  static const List<String> _products = const [
    '/SpacerWidget',
  ];
  Widget _buildProductItem(BuildContext context, int index) {
    return Container(
      child: FlatButton(
        onPressed: () {
          print("_buildProductItem >> _products > index.toString() :: " + index.toString() + " , _products[index] : " + _products[index]);
          Navigator.of(context).pushNamed(_products[index]);
        },
        child: Align(
          alignment: Alignment.centerLeft,
          child: Container(
            padding: EdgeInsets.only(top: 22, bottom: 22, right: 22),
            child: Row(children: [
              RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: _products[index],
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Colors.grey,
                        fontStyle: FontStyle.normal,
                        fontSize: 18.0,
                      ),
                    ),
                  ],
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: _buildProductItem,
      itemCount: _products.length,
    );
  }

}