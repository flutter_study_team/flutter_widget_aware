import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_widget_aware/widgets/absorb_pointer/absorb_pointer_page.dart';
import 'package:flutter_widget_aware/widgets/absorb_pointer/absorb_pointer_widget.dart';
import 'package:flutter_widget_aware/widgets/align/align_page.dart';
import 'package:flutter_widget_aware/widgets/align/align_widget.dart';
import 'package:flutter_widget_aware/widgets/animated_build/animated_builder_page.dart';
import 'package:flutter_widget_aware/widgets/animated_build/animated_builder_widget.dart';
import 'package:flutter_widget_aware/widgets/animated_container/animated_alignment.dart';
import 'package:flutter_widget_aware/widgets/animated_container/animated_container.dart';
import 'package:flutter_widget_aware/widgets/animated_container/animated_gradient_transform.dart';
import 'package:flutter_widget_aware/widgets/animated_container/animated_properties.dart';
import 'package:flutter_widget_aware/widgets/animated_icon_widget/animated_icon.dart';
import 'package:flutter_widget_aware/widgets/animated_icon_widget/animated_icon_page.dart';
import 'package:flutter_widget_aware/widgets/animated_list/animated_list_page.dart';
import 'package:flutter_widget_aware/widgets/animated_list/animated_list_widget.dart';
import 'package:flutter_widget_aware/widgets/animated_padding/animated_padding.dart';
import 'package:flutter_widget_aware/widgets/animated_padding/animated_padding_page.dart';
import 'package:flutter_widget_aware/widgets/animated_positioned_widget/animated_positioned.dart';
import 'package:flutter_widget_aware/widgets/animated_positioned_widget/animated_positioned_page.dart';
import 'package:flutter_widget_aware/widgets/animated_switcher/animated_switcher.dart';
import 'package:flutter_widget_aware/widgets/animated_switcher/animated_switcher_page.dart';
import 'package:flutter_widget_aware/widgets/aspect_ratio/aspect_ratio.dart';
import 'package:flutter_widget_aware/widgets/aspect_ratio/aspect_ratio_page.dart';
import 'package:flutter_widget_aware/widgets/backdrop_filter/backdrop_filter_page.dart';
import 'package:flutter_widget_aware/widgets/backdrop_filter/backdrop_filter_widget.dart';
import 'package:flutter_widget_aware/widgets/clip_r_rect/clip_r_rect_page.dart';
import 'package:flutter_widget_aware/widgets/clip_r_rect/clip_r_rect_widget.dart';
import 'package:flutter_widget_aware/widgets/custom_paint/custom_paint_page.dart';
import 'package:flutter_widget_aware/widgets/custom_paint/custom_paint_widget.dart';
import 'package:flutter_widget_aware/widgets/dismissible/dismissible_page.dart';
import 'package:flutter_widget_aware/widgets/dismissible/dismissible_widget.dart';
import 'package:flutter_widget_aware/widgets/draggable/draggable_page.dart';
import 'package:flutter_widget_aware/widgets/draggable/draggable_widget.dart';
import 'package:flutter_widget_aware/widgets/expanded/expanded_column.dart';
import 'package:flutter_widget_aware/widgets/expanded/expanded_page.dart';
import 'package:flutter_widget_aware/widgets/expanded/expanded_sight.dart';
import 'package:flutter_widget_aware/widgets/fade_in_image/fade_in_image.dart';
import 'package:flutter_widget_aware/widgets/fade_in_image/fade_in_image_page.dart';
import 'package:flutter_widget_aware/widgets/fade_transition/fade_transition_curved.dart';
import 'package:flutter_widget_aware/widgets/fade_transition/fade_transition_move_to_screen.dart';
import 'package:flutter_widget_aware/widgets/fade_transition/fade_transition_page.dart';
import 'package:flutter_widget_aware/widgets/fade_transition/fade_transition_swipe.dart';
import 'package:flutter_widget_aware/widgets/fade_transition/fade_transition_tween.dart';
import 'package:flutter_widget_aware/widgets/fitted_box/fitted_box_page.dart';
import 'package:flutter_widget_aware/widgets/fitted_box/fitted_box_widget.dart';
import 'package:flutter_widget_aware/widgets/flexible/flexible_page.dart';
import 'package:flutter_widget_aware/widgets/flexible/flexible_widget.dart';
import 'package:flutter_widget_aware/widgets/floating_action_button/fab_multy.dart';
import 'package:flutter_widget_aware/widgets/floating_action_button/fab_page.dart';
import 'package:flutter_widget_aware/widgets/floating_action_button/fab_with_navigationbar.dart';
import 'package:flutter_widget_aware/widgets/future_builder/future_builder_app_documents.dart';
import 'package:flutter_widget_aware/widgets/future_builder/future_builder_delay.dart';
import 'package:flutter_widget_aware/widgets/future_builder/future_builder_lsit.dart';
import 'package:flutter_widget_aware/widgets/future_builder/future_builder_page.dart';
import 'package:flutter_widget_aware/widgets/hero/hero_page.dart';
import 'package:flutter_widget_aware/widgets/hero/hero_widget.dart';
import 'package:flutter_widget_aware/widgets/hero/hero_widget_2.dart';
import 'package:flutter_widget_aware/widgets/indexed_stack/indexed_stack.dart';
import 'package:flutter_widget_aware/widgets/indexed_stack/indexed_stack_page.dart';
import 'package:flutter_widget_aware/widgets/inherited_model/inherited_model_page.dart';
import 'package:flutter_widget_aware/widgets/inherited_model/inherited_model_widget.dart';
import 'package:flutter_widget_aware/widgets/inherited_widget/inherited.dart';
import 'package:flutter_widget_aware/widgets/inherited_widget/inherited_widget_page.dart';
import 'package:flutter_widget_aware/widgets/layout_builder/layout_builder_page.dart';
import 'package:flutter_widget_aware/widgets/layout_builder/layout_builder_widget.dart';
import 'package:flutter_widget_aware/widgets/limited_box/limited_box.dart';
import 'package:flutter_widget_aware/widgets/limited_box/limited_box_page.dart';
import 'package:flutter_widget_aware/widgets/media_query/media_query_page.dart';
import 'package:flutter_widget_aware/widgets/media_query/media_query_widget.dart';
import 'package:flutter_widget_aware/widgets/opacity/opacity_just.dart';
import 'package:flutter_widget_aware/widgets/opacity/opacity_page.dart';
import 'package:flutter_widget_aware/widgets/opacity/opacity_with_animated.dart';
import 'package:flutter_widget_aware/widgets/pageview/pageview_on_list_item.dart';
import 'package:flutter_widget_aware/widgets/pageview/pageview_page.dart';
import 'package:flutter_widget_aware/widgets/pageview/pageview_with_navigator.dart';
import 'package:flutter_widget_aware/widgets/placeholder/placeholder.dart';
import 'package:flutter_widget_aware/widgets/placeholder/placeholder_page.dart';
import 'package:flutter_widget_aware/widgets/positioned/positioned_page.dart';
import 'package:flutter_widget_aware/widgets/positioned/positioned_widget.dart';
import 'package:flutter_widget_aware/widgets/reorderable_listview/reorderable_listiew.dart';
import 'package:flutter_widget_aware/widgets/reorderable_listview/reorderable_listview_page.dart';
import 'package:flutter_widget_aware/widgets/rich_text/rich_text.dart';
import 'package:flutter_widget_aware/widgets/rich_text/rich_text_page.dart';
import 'package:flutter_widget_aware/widgets/safe_area/safe_area.dart';
import 'package:flutter_widget_aware/widgets/safe_area/safe_area_widget.dart';
import 'package:flutter_widget_aware/widgets/semantics/semantics_page.dart';
import 'package:flutter_widget_aware/widgets/semantics/semantics_widget.dart';
import 'package:flutter_widget_aware/widgets/size_box/sized_box_page.dart';
import 'package:flutter_widget_aware/widgets/size_box/sized_box_widget.dart';
import 'package:flutter_widget_aware/widgets/sliver_app_bar/sliver_app_bar.dart';
import 'package:flutter_widget_aware/widgets/sliver_app_bar/sliver_app_bar_grid.dart';
import 'package:flutter_widget_aware/widgets/sliver_app_bar/sliver_app_bar_list.dart';
import 'package:flutter_widget_aware/widgets/sliver_app_bar/sliver_app_bar_page.dart';
import 'package:flutter_widget_aware/widgets/spacer/spacer_page.dart';
import 'package:flutter_widget_aware/widgets/spacer/spacer_widget.dart';
import 'package:flutter_widget_aware/widgets/stream_builder/stream_builder.dart';
import 'package:flutter_widget_aware/widgets/stream_builder/stream_builder_page.dart';
import 'package:flutter_widget_aware/widgets/table/table.dart';
import 'package:flutter_widget_aware/widgets/tooltip/tooltip_page.dart';
import 'package:flutter_widget_aware/widgets/tooltip/tooltip_widget.dart';
import 'package:flutter_widget_aware/widgets/transform/transform_page.dart';
import 'package:flutter_widget_aware/widgets/transform/transform_widget.dart';
import 'package:flutter_widget_aware/widgets/value_listenable_builder/value_listenable_builder_page.dart';
import 'package:flutter_widget_aware/widgets/value_listenable_builder/value_listenable_builder_widget.dart';
import 'package:flutter_widget_aware/widgets/wrap/wrap.dart';
import 'package:flutter_widget_aware/widgets/wrap/wrap_direction_horizontal.dart';
import 'package:flutter_widget_aware/widgets/wrap/wrap_direction_vertical.dart';

/*
 * https://www.youtube.com/playlist?list=PLjxrf2q8roU23XGwz3Km7sQZFTdB996iG
 */
class FlutterWidgetApp extends StatefulWidget {
  @override
  State createState() {
    return _FlutterWidgetAppState();
  }
}

class _FlutterWidgetAppState extends State<FlutterWidgetApp> with WidgetsBindingObserver{

  Timer _timerLink;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    if (_timerLink != null) {
      _timerLink.cancel();
    }
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print("didChangeAppLifecycleState :: " + state.toString());
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FLUTTER WIDGET',
      theme: ThemeData(
        primaryColor: const Color(0xFFF2BF3F),
        primaryColorLight: const Color(0xFFF7E0AA),
      ),
      home: HomePage(),
      routes: <String, WidgetBuilder> {
        '/Home': (BuildContext context) => HomePage(),
        '/SafeArea': (BuildContext context) => SafeAreaPage(),
        '/SafeAreaWidget': (BuildContext context) => SafeAreaWidget(),
        '/Expanded': (BuildContext context) => ExpandedPage(),
        '/ExpandedSite': (BuildContext context) => ExpandedSite(),
        '/ExpandedColumn': (BuildContext context) => ExpandedColumn(),
        '/Wrap': (BuildContext context) => WrapPage(),
        '/WrapDirectionHorizontal': (BuildContext context) => WrapDirectionHorizontal(),
        '/WrapDirectionVertical': (BuildContext context) => WrapDirectionVertical(),
        '/AnimatedContainer': (BuildContext context) => AnimatedContainerPage(),
        '/AnimatedProperties': (BuildContext context) => AnimatedProperties(),
        '/AnimatedAlignment': (BuildContext context) => AnimatedAlignment(),
        '/AnimatedGradientTransform': (BuildContext context) => AnimatedGradientTransform(),
        '/Opacity': (BuildContext context) => OpacityPage(),
        '/OpacityJust': (BuildContext context) => OpacityJust(),
        '/OpacityWithAnimated': (BuildContext context) => OpacityWithAnimated(),
        '/FutureBuilder': (BuildContext context) => FutureBuilderPage(),
        '/FutureBuilderDelay': (BuildContext context) => FutureBuilderDelay(),
        '/FutureBuilderList': (BuildContext context) => FutureBuilderList(),
        '/FutureBuilderAppDocuments': (BuildContext context) => FutureBuilderAppDocuments(),
        '/FadeTransition': (BuildContext context) => FadeTransitionPage(),
        '/Swipe': (BuildContext context) => FadeTransitionSwipe(),
        '/Curved': (BuildContext context) => FadeTransitionCurved(),
        '/Tween': (BuildContext context) => FadeTransitionTween(),
        '/MoveToScreenPage': (BuildContext context) => FadeTransitionMoveToScreenPage(),
        '/FloatingActionButton': (BuildContext context) => FABPage(),
        '/FloatingActionButtonMulty': (BuildContext context) => FABMulty(),
        '/FABNavigationBar': (BuildContext context) => FABNavigationBar(),
        '/PageView': (BuildContext context) => PageViewPage(),
        '/PageViewWithNavigor': (BuildContext context) => PageViewWithNavigor(),
        '/PageViewOnListItem': (BuildContext context) => PageViewOnListItem(),
        '/TablePage': (BuildContext context) => TablePage(),
        '/SliverAppBarPage': (BuildContext context) => SliverAppBarPage(),
        '/SliverAppBarWidget': (BuildContext context) => SliverAppBarWidget(),
        '/SliverAppBarList': (BuildContext context) => SliverAppBarList(),
        '/SliverAppBarGrid': (BuildContext context) => SliverAppBarGrid(),
        '/FadeInImagePage': (BuildContext context) => FadeInImagePage(),
        '/FadeInImageWidget': (BuildContext context) => FadeInImageWidget(),
        '/StreamBuilderPage': (BuildContext context) => StreamBuilderPage(),
        '/StreamBuilderWidget': (BuildContext context) => StreamBuilderWidget(),

        '/InheritedModelPage': (BuildContext context) => InheritedModelPage(),
        '/InheritedModelWidget': (BuildContext context) => InheritedModelWidget(),
        '/ClipRRectPage': (BuildContext context) => ClipRRectPage(),
        '/ClipRRectWidget': (BuildContext context) => ClipRRectWidget(),
        '/HeroPage': (BuildContext context) => HeroPage(),
        '/HeroWidget': (BuildContext context) => HeroWidget(),
        '/HeroWidget2': (BuildContext context) => HeroWidget2(),
        '/CustomPaintPage': (BuildContext context) => CustomPaintPage(),
        '/CustomPaintWidget': (BuildContext context) => CustomPaintWidget(),
        '/ToolTipPage': (BuildContext context) => ToolTipPage(),
        '/TooltipWidget': (BuildContext context) => TooltipWidget(),
        '/FittedBoxPage': (BuildContext context) => FittedBoxPage(),
        '/FittedBoxWidget': (BuildContext context) => FittedBoxWidget(),
        '/LayoutBuilderPage': (BuildContext context) => LayoutBuilderPage(),
        '/LayoutBuilderWidget': (BuildContext context) => LayoutBuilderWidget(),

        '/AbsorbPointerPage': (BuildContext context) => AbsorbPointerPage(),
        '/AbsorbPointerWidget': (BuildContext context) => AbsorbPointerWidget(),
        '/TransFormPage': (BuildContext context) => TransFormPage(),
        '/TransformWidget': (BuildContext context) => TransformWidget(),
        '/BackDropFilterPage': (BuildContext context) => BackDropFilterPage(),
        '/BackDropFilterWidget': (BuildContext context) => BackDropFilterWidget(),
        '/AlignPage': (BuildContext context) => AlignPage(),
        '/AlignWidget': (BuildContext context) => AlignWidget(),
        '/PositionedPage': (BuildContext context) => PositionedPage(),
        '/PositionedWidget': (BuildContext context) => PositionedWidget(),
        '/AnimatedBuilderPage': (BuildContext context) => AnimatedBuilderPage(),
        '/AnimatedBuilderWidget': (BuildContext context) => AnimatedBuilderWidget(),
        '/DismissiblePage': (BuildContext context) => DismissiblePage(),
        '/DismissibleWidget': (BuildContext context) => DismissibleWidget(),

        '/SizedBoxPage': (BuildContext context) => SizedBoxPage(),
        '/SizedBoxWidget': (BuildContext context) => SizedBoxWidget(),
        '/ValueListenableBuilderPage': (BuildContext context) => ValueListenableBuilderPage(),
        '/ValueListenableBuilderWidget': (BuildContext context) => ValueListenableBuilderWidget(),
        '/DraggablePage': (BuildContext context) => DraggablePage(),
        '/DraggableWidget': (BuildContext context) => DraggableWidget(),
        '/AnimatedListPage': (BuildContext context) => AnimatedListPage(),
        '/AnimatedListWidget': (BuildContext context) => AnimatedListWidget(),
        '/FlexiblePage': (BuildContext context) => FlexiblePage(),
        '/FlexibleWidget': (BuildContext context) => FlexibleWidget(),
        '/MediaQueryPage': (BuildContext context) => MediaQueryPage(),
        '/MediaQueryWidget': (BuildContext context) => MediaQueryWidget(),
        '/SpacerPage': (BuildContext context) => SpacerPage(),
        '/SpacerWidget': (BuildContext context) => SpacerWidget(),

        '/InheritedWidgetPage': (BuildContext context) => InheritedWidgetPage(),
        '/InheritedWidgett': (BuildContext context) => InheritedWidgett(),
        '/AnimatedIconPage': (BuildContext context) => AnimatedIconPage(),
        '/AnimatedIconWidget': (BuildContext context) => AnimatedIconWidget(),
        '/AspectRatioPage': (BuildContext context) => AspectRatioPage(),
        '/AspectRatioWidget': (BuildContext context) => AspectRatioWidget(),
        '/LimitedBoxPage': (BuildContext context) => LimitedBoxPage(),
        '/LimitedBoxWidget': (BuildContext context) => LimitedBoxWidget(),
        '/PlaceholderPage': (BuildContext context) => PlaceholderPage(),
        '/PlaceholderWidget': (BuildContext context) => PlaceholderWidget(),
        '/RichTextPage': (BuildContext context) => RichTextPage(),
        '/RichTextWidget': (BuildContext context) => RichTextWidget(),
        '/ReorderableListViewPage': (BuildContext context) => ReorderableListViewPage(),
        '/ReorderableListViewWidget': (BuildContext context) => ReorderableListViewWidget(),

        '/AnimatedSwitcherPage': (BuildContext context) => AnimatedSwitcherPage(),
        '/AnimatedSwitcherWidget': (BuildContext context) => AnimatedSwitcherWidget(),
        '/AnimatedPositionedPage': (BuildContext context) => AnimatedPositionedPage(),
        '/AnimatedPositionedWidget': (BuildContext context) => AnimatedPositionedWidget(),
        '/AnimatedPaddingPage': (BuildContext context) => AnimatedPaddingPage(),
        '/AnimatedPaddingWidget': (BuildContext context) => AnimatedPaddingWidget(),
        '/IndexedStackPage': (BuildContext context) => IndexedStackPage(),
        '/IndexedStackWidget': (BuildContext context) => IndexedStackWidget(),
        '/SemanticsPage': (BuildContext context) => SemanticsPage(),
        '/SemanticsWidget': (BuildContext context) => SemanticsWidget(),


      },
    );
  }
}


class HomePage extends StatefulWidget {
  final String title = 'Flutter Widget';
  @override
  State createState() => HomePageState();
}

class HomePageState extends State<HomePage> {

  static const List<String> _products = const [
    '/SafeArea',
    '/Expanded',
    '/Wrap',
    '/AnimatedContainer',
    '/Opacity',
    '/FutureBuilder',
    '/FadeTransition',

    '/FloatingActionButton',
    '/PageView',
    '/TablePage',
    '/SliverAppBarPage',
    '/FadeInImagePage',
    '/StreamBuilderPage',

    '/InheritedModelPage',
    '/ClipRRectPage',
    '/HeroPage',
    '/CustomPaintPage',
    '/TooltipWidget',
    '/FittedBoxPage',
    '/LayoutBuilderPage',

    '/AbsorbPointerPage',
    '/TransFormPage',
    '/BackDropFilterPage',
    '/AlignPage',
    '/PositionedPage',
    '/AnimatedBuilderPage',
    '/DismissiblePage',

    '/SizedBoxPage',
    '/ValueListenableBuilderPage',
    '/DraggablePage',
    '/AnimatedListPage',
    '/FlexiblePage',
    '/MediaQueryPage',
    '/SpacerPage',

    '/InheritedWidgetPage',
    '/AnimatedIconPage',
    '/AspectRatioPage',
    '/LimitedBoxPage',
    '/PlaceholderPage',
    '/RichTextPage',
    '/ReorderableListViewPage',


    '/AnimatedSwitcherPage',
    '/AnimatedPositionedPage',
    '/AnimatedPaddingPage',
    '/IndexedStackPage',
    '/SemanticsPage',

  ];


  Widget _buildProductItem(BuildContext context, int index) {
    return Container(
      child: FlatButton(
        onPressed: () {
          print("_buildProductItem >> _products > index.toString() :: " + index.toString() + " , _products[index] : " + _products[index]);
          Navigator.of(context).pushNamed(_products[index]);
        },
        child: Align(
          alignment: Alignment.centerLeft,
          child: Container(
            padding: EdgeInsets.only(top: 22, bottom: 22, right: 22),
            child: Row(children: [
              RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                      text: _products[index],
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Colors.grey,
                        fontStyle: FontStyle.normal,
                        fontSize: 18.0,
                      ),
                    ),
                  ],
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: AppBar(
          title: RichText(
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(
                  text: "FLUTTER WIDGET",
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.grey,
                    fontStyle: FontStyle.normal,
                    fontSize: 18.0,
                  ),
                ),
              ],
            ),
          ),
        ),
        body: Container(
          child: ListView.builder(
            itemBuilder: _buildProductItem,
            itemCount: _products.length,
          ),
        ),
      ),
    );
  }
}